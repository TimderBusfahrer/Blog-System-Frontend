var express = require('express');
var router = express.Router();

router.get('/', (req, res, next) => {
  res.render('index', {
    pageinfo: 'Hier findest du eine Übersicht über alle Beiträge', 
    centerheading: 'Alle Beiträge'
  });
});

router.get('/about', (req, res, next) => {
  res.render('about', {
    pageinfo: 'Hier findest du ein paar Informationen über uns',
    centerheading: 'Informationen im Überblick'
  })
});

router.get('/admin/login', (req, res, next) => {
  res.render('adminLogin', {
    pageinfo: 'Hier kannst du dich mit einem Zugang als Administrator',
    centerheading: 'Admin-Zugang'
  });
});

router.get('/admin/index', (req, res, next) => {
  res.render('adminIndex', {
    pageinfo: 'Hier kannst du deinen Blog administrieren',
    centerheading: 'Admin-Informationen'
  });
});

module.exports = router;